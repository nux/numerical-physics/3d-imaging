#!/usr/bin/env python3 

import numpy as np
import time
import os

np.random.seed(int(time.time()))


def init_pop(popsize, sigma):
    population = []
    nshapes=len(bounds['shape'])
    
    for i in range(popsize):
        element={'shape':[],
                'position':[],
                'additional':[]}

        for ishape in range(nshapes):
            shapebounds=bounds['shape'][ishape]
            shape={}
            for key in shapebounds:
                if key !='points':
                    shape[key]=  (shapebounds[key][1]-shapebounds[key][0])*np.random.rand() + shapebounds[key][0]
                    #print(shape[key])
            
            res = shapebounds["points"].shape[0]
            shape["points"]=np.zeros([res,3])

            meanrad=np.random.rand()*(rbounds[1]-rbounds[0]) + rbounds[0]
            radii=np.random.normal(loc=meanrad, scale=sigma*meanrad, size=res) 
        
            radii=np.clip(radii, rbounds[0], rbounds[1])
            
            points=np.random.normal(size=(res,3))
            for i in range(res):
                shape["points"][i,:]=points[i,:]/(np.sum(points[i,:]**2))**0.5 * radii[i]

            pos=np.random.rand(3)*(bounds['position'][ishape][:,1]-bounds['position'][ishape][:,0])+bounds['position'][ishape][:,0]

            element['shape'].append(shape)
            element['position'].append(pos)
        
        
        addbounds=bounds['additional']
        add={}
        for key in addbounds:
            add[key]=  (addbounds[key][1]-addbounds[key][0])*np.random.rand() + addbounds[key][0]
        
        element['additional']=add

        population.append(element)
        
        
        
    return population

def create_bounds(nfaces, nshapes):

    addbounds={'scaling':scalebounds,
               'offset': offsetbounds,
                'xshift':shiftbounds,
                'yshift':shiftbounds}

    bounds={'shape':[],
            'position':[],
            'additional':addbounds,
            '_radius': rbounds}
    
    for ishape in range(nshapes):
    
    
        pointsbounds = np.zeros([nfaces,3,2])
        pointsbounds[:,:,0]=coordsbounds[0]
        pointsbounds[:,:,1]=coordsbounds[1]
        
        parambounds={
                'points': pointsbounds,
                'max_radius': [-1, -1],
                'delta': deltabounds,
                'beta': betabounds}
        
        pos = np.zeros([3,2])
        pos[:,0]=posbounds[0]
        pos[:,1]=posbounds[1]        
        
        bounds['shape'].append(parambounds)
        bounds['position'].append(pos)

    

    
    return bounds        

####################################################d

from imaging3Dtools.optimizer import Optimizer
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("filename", help='name of the file that contains the experimental data')
parser.add_argument('-g', '--gpu', help='delimited list of gpus to use (e.g. give 0,2 to use gpus 0 and 2 for the simulations)', type=str)
parser.add_argument('-p', '--popsize', help='population size', type=int)
parser.add_argument('-ns', '--nshapes', help='number of shapes. For individual clusters, it is set to 1 (default). For agglomerates, it can be 2 or 3.', type=int)
parser.add_argument('-nf', '--nfaces', help='number of faces per shape. Default is 30.', type=int)
parser.add_argument('-r', '--radius', help='comma-separated minimum and maximum radius for the shapes initialization, expressed in units of the wavelength. For example, if the wavelength is 5nm, -r 2,10 means that the shapes are initialized randomly with radii between 10nm and 50nm', type=str)
parser.add_argument('-md', '--maxdist', help='maximum distance between shapes (it has an effect only if nshapes>1), expressed in units of wavelength.', type=float)


args = parser.parse_args()


if args.gpu is None:
    gpulist=[0]
else:
    gpulist = [int(item) for item in args.gpu.split(',')]


filename = args.filename


gpulist = [0]        if args.gpu      is None else [int(item) for item in args.gpu.split(',')]
popsize = 8000      if args.popsize  is None else args.popsize
nshapes = 1         if args.nshapes  is None else args.nshapes
nfaces = 30          if args.nfaces   is None else args.nfaces
rbounds = [1,10]    if args.radius   is None else [float(item) for item in args.radius.split(',')]
maxdist = 20        if args.maxdist  is None else args.maxdist




###### Parameters bounds ###############################


coordsbounds=[-rbounds[1],rbounds[1]]

betabounds=[1e-5,6e-3]

deltabounds=[0.011,0.0125]

posbounds=[-maxdist, maxdist]

shiftbounds=[-1.2, 1.2]

scalebounds=[0.8,1.]

offsetbounds=[8,13]

dataid = os.path.basename(filename).split(".")[0]

print("dataid")


myopt = Optimizer(
    seed =          int(time.time()),
    workers=        8,             # Set the number of python processes for parallel operations
    tag=            dataid,          # Set the tag for the output file. The result will be written on file "fit_result_[tag].h5"
    save_steps=     False            #Save steps in the output folder

    )

myopt.read_data(
    dataname=       filename, 

    center=         [553,540] , 
    cutsize=        -1,
    resolution=     128,  
    detdistance=    70., 
    pixelsize=      0.075, 
    satlevel=       2.e4, 
    minlevel=       0
    )

myopt.set_simulation(
    sim_resolution= 256,            # resolution at which the simulation is actually performed
    gpuids =        gpulist,            # GPU id to use for the simulation
    threads=        8               # Number of threads used for the simulation
    )

myopt.set_data(
    error_exp=      0.5,            # Set the error metric. 
    error_metric=   1
    )

myopt.set_genetic_parameters(
    generations=    200,            # Number of generations
    crossexp=       1.,            # NOT USED FOR NOW
    crossbal=       0.66,            # Crossover balance
    crossdiff=      [0.3,0.4]       # Crossover differential coefficient
    )

myopt.set_simplex_parameters(
    maxfev=         30,             # Number of target evaluations per optimization parameter
    simplex_step=   [0.01, 0.04],    # Simplex step
    simplex_pop_step= [0.33, 0.33],    # Simplex step derived from population
    nopt=           16,              # Number of shapes to optimize
    nopt_halfsteps= 40,
    nopt_min=       4,
    maxopt=         3,
    parallel=       True            # Choose wether or not the parallel neldermead version must be used. If False, use the SciPy implementation
    )

##################################################d

bounds=create_bounds(               # This function defines the bounds, and it is declared above in this file
    nfaces=        nfaces,              # Number of faces of the shape
    nshapes=        nshapes
        )
population=init_pop(                # This function initializes the population, and it is declared above in this file
    popsize=        2*popsize,           # Population size
    sigma=          0.001
        ) 
myopt.set_population(               
    bounds=         bounds,                  # Set the previously created bounds
    population=     population,          # Set the previously created population
    popsize=        popsize
    )


myopt.optimize()


