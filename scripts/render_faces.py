#!/usr/bin/env python3
import vtk
from vtk.util import numpy_support

import scatman
import numpy as np
import time
import scipy.ndimage.filters as filters
import os

isovalue = 0.5


class Scene:
    wavelength=60.
    reduction=2
    screenshot_counter=1

    
    def show_density(self):
        local_dx=self.wavelength*self.dx
        density=self.shapes[self.index].get(local_dx)
        density=filters.gaussian_filter(density,0.8)
        #size=self.shapes[self.index].get_size()
        print(density.shape)
        density = density/np.amax(density)
        
        VTK_data = numpy_support.numpy_to_vtk(num_array=density.flatten(), deep=True, array_type=vtk.VTK_DOUBLE)
    
        spacing = 1./density.shape[0]*self.reduction
        origin=[-0.5*self.reduction, -0.5*self.reduction, -0.5*self.reduction]
        self.imdata.SetDimensions(density.shape[::-1])
        self.imdata.SetSpacing([spacing,spacing,spacing])
        self.imdata.SetOrigin(origin)
        self.imdata.GetPointData().SetScalars(VTK_data)
        self.imdata.Modified()
        
    def show_pattern(self):
        pattern=self.patterns[self.index]
        maxval=np.amax(pattern)
        
        pattern/=maxval
        
        pattern= np.log(np.array(pattern))
        
        cutlevel=-15
        minval=np.amin(pattern[pattern>-20])
        
        if(minval>cutlevel):
            pattern-=minval
            pattern[pattern<0]=0
        else:
            pattern-=cutlevel
            pattern[pattern<0]=0
        
        pattern[pattern<0]=0
        
        
        pattern/=np.amax(pattern)
        
        VTK_pattern = numpy_support.numpy_to_vtk(num_array=pattern[::-1, ::-1].flatten(), deep=True, array_type=vtk.VTK_DOUBLE)        
        
        self.impattern.SetDimensions((pattern.shape[0], pattern.shape[1], 1))
        self.impattern.SetSpacing([3./pattern.shape[0],3./pattern.shape[1],1])
        self.impattern.GetPointData().SetScalars(VTK_pattern)
        self.impattern.Modified()
    
    
    def get_screenshot(self):
        windowToImageFilter = vtk.vtkWindowToImageFilter()
        windowToImageFilter.SetInput(self.window);
        windowToImageFilter.SetScale(1,1); #set the resolution of the output image (3 times the current resolution of vtk render window)
        windowToImageFilter.SetInputBufferTypeToRGB(); #also record the alpha (transparency) channel
        windowToImageFilter.ReadFrontBufferOff(); # read from the back buffer
        windowToImageFilter.Update();


        screenshot_name=os.path.join(self.screenshot_folder, "screenshot_{:03d}.png".format(self.screenshot_counter))
        print("Saving screenshot to ", screenshot_name)
        writer = vtk.vtkPNGWriter();
        writer.SetFileName(screenshot_name);
        writer.SetInputConnection(windowToImageFilter.GetOutputPort());
        writer.Write();
        windowToImageFilter.Update();
        self.screenshot_counter+=1
    
    def keypress_callback(self, obj, ev):
        key = obj.GetKeySym()
        print(key, 'was pressed')
        if key=="Right" or key=="Left":
            if key=="Right":
                self.index=self.index+1
                if self.index >=len(self.patterns):
                    self.index=0
            if key=="Left":
                self.index=self.index-1
                if self.index<0:
                    self.index=len(self.patterns)-1
            print(self.index)
            self.show_density()
            self.show_pattern()
            self.window.Render()
        if key == "Escape":
            self.interactor.GetRenderWindow().Finalize()
            self.interactor.TerminateApp()

        if key == "Return":        
            self.get_screenshot()

    
    def __init__(self, in_patterns, in_shapes, wavelength_in=1., dx=0.33, render_pattern=False, render_beam=False, screenshot_folder=".", screenshot_views=[]):
        self.screenshot_folder=screenshot_folder
        self.dx=dx
        self.wavelength=wavelength_in
        self.shapes=in_shapes

        self.patterns=in_patterns
            
        p0=[0.0, 0.0, 5.0]
        p1=[0.0, 0.0, -3.0]
        lineSource=vtk.vtkLineSource()
        lineSource.SetPoint1(p0);
        lineSource.SetPoint2(p1);
        lineSource.Update();
        
        tubeFilter = vtk.vtkTubeFilter();
        tubeFilter.SetInputConnection(lineSource.GetOutputPort());
        tubeFilter.SetRadius(.025); 
        tubeFilter.SetNumberOfSides(50);
        tubeFilter.Update();
        
        
        self.index=0;

        self.imdata = vtk.vtkImageData()

        #self.imdata.SetOrigin([-1,-1,-1])
        

        self.impattern = vtk.vtkImageData()
        # fill the vtk image data object

        self.impattern.SetOrigin([-1.5,-1.5, 5])


        self.ltpattern=vtk.vtkLookupTable()
        self.ltpattern.SetScaleToLinear()
        self.ltpattern.SetTableRange(0, 1)

        ncolors=256
        self.ltpattern.SetNumberOfColors(ncolors)

        for i in range(0,ncolors):
            val = float(i+1)/float(ncolors)
            red = min(3.*val,1)
            green = min(0 if val<1./3. else 3.*(val-1./3.), 1)
            blue = 0 if val< 2./3. else 3.*(val-2./3.)
            self.ltpattern.SetTableValue(i, red, green, blue)


        self.mcpattern = vtk.vtkImageMapToColors()
        self.mcpattern.SetLookupTable(self.ltpattern)
        self.mcpattern.SetInputData(self.impattern)


        self.surface = vtk.vtkMarchingCubes()
        self.surface.SetInputData(self.imdata);
        self.surface.ComputeNormalsOn();
        self.surface.SetValue(0, isovalue);


        ###############################################
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputConnection(self.surface.GetOutputPort())

        self.linemapper = vtk.vtkPolyDataMapper()  
        self.linemapper.SetInputConnection(tubeFilter.GetOutputPort())
        
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        self.actor.GetMapper().ScalarVisibilityOff()
        self.actor.GetProperty().SetAmbient(0.0);
        self.actor.GetProperty().SetDiffuse(0.9);
        #self.actor.GetProperty().SetColor(0.8, 0.8, 1);
        self.actor.GetProperty().SetColor(174/245., 209/256., 207/256.);
        #self.actor.GetProperty().SetRoughness(1.)

        #self.actor.GetProperty().SetAmbientColor(1,0,0)
        
        self.actor2 = vtk.vtkImageActor()
        self.actor2.GetMapper().SetInputConnection(self.mcpattern.GetOutputPort())
        self.actor2.GetProperty().SetAmbient(1);
        self.actor2.GetProperty().SetDiffuse(0);
        #self.actor.GetProperty().SetColor(174/245., 209/256., 207/256.);

        self.lineactor=vtk.vtkActor()
        self.lineactor.SetMapper(self.linemapper);
        self.lineactor.GetProperty().SetLineWidth(20);        
        self.lineactor.GetProperty().SetColor(1,0,0); 
        self.lineactor.GetProperty().SetOpacity(0.15)
        
        self.window = vtk.vtkRenderWindow()
        self.window.SetSize(800, 800)

        self.interactor = vtk.vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(self.window)
        self.interactor.AddObserver('KeyPressEvent', self.keypress_callback, 1.0)

        self.camera = vtk.vtkCamera()
        self.camera.SetPosition(-3.5, 1., -6);
        self.camera.SetFocalPoint(0, 0, 0);
        self.camera.SetParallelProjection(1)

        self.renderer = vtk.vtkRenderer()

        #lk = vtk.vtkLightKit()
        #lk.RemoveLightsFromRenderer(renderer)

        self.renderer.SetActiveCamera(self.camera);

        self.window.AddRenderer(self.renderer)

        self.light = vtk.vtkLight()
        self.light.SetLightTypeToSceneLight()
        self.light.SetPosition(0,0,-8)
        #light.SetPositional(True)
        #self.light.SetConeAngle(30)
        #self.light.SetFocalPoint(0, 0, 0)
        #self.light.SetDiffuseColor(1,0.8,0.8)
        #self.light.SetAmbientColor(1,0.8,0.8)
        #self.light.SetSpecularColor(0.,0.,0.)
        
        self.light.SetConeAngle(180)
        self.light.SetFocalPoint(0, 0, 0)
        self.light.SetDiffuseColor(1,1,1)
        self.light.SetAmbientColor(1,1,1)
        self.light.SetSpecularColor(1,1,1)
        
   
        self.light2 = vtk.vtkLight()
        self.light2.SetLightTypeToSceneLight()
        self.light2.SetPosition(0,0,8)
        #light.SetPositional(True)
        #self.light2.SetConeAngle(30)
        self.light2.SetFocalPoint(0, 0, 0)
        self.light2.SetDiffuseColor(1,0.,0.)
        self.light2.SetAmbientColor(1,0.,0.)
        self.light2.SetSpecularColor(1,1,1)

        self.light3 = vtk.vtkLight()
        self.light3.SetLightTypeToHeadlight()
        #self.light3.SetPosition(0,0,8)
        #light.SetPositional(True)
        #self.light3.SetConeAngle(30)
        #self.light3.SetFocalPoint(0, 0, 0)
        self.light3.SetDiffuseColor(1,1,1)
        self.light3.SetAmbientColor(1,1,1)
        self.light3.SetSpecularColor(1,1,1)
        self.light3.SetIntensity(0.4)

        self.renderer.AddActor(self.actor)
        if render_pattern:
            self.renderer.AddActor(self.actor2)
        if render_beam:
            self.renderer.AddActor(self.lineactor)

        self.renderer.AddLight(self.light)
        self.renderer.AddLight(self.light2)
        self.renderer.AddLight(self.light3)

        # Setting the background to blue.
        #self.renderer.SetBackground(0.25, 0.25, 0.25)
        self.renderer.SetBackground(1, 1, 1)


        self.show_density()
        self.show_pattern()
        
        

        if len(screenshot_views)==0:
            self.window.Render()
            self.interactor.Start()

        else:
            for view in screenshot_views:
                self.window.SetOffScreenRendering(1);
                self.window.Render()
                self.camera.SetPosition(*view)
                self.get_screenshot()
    
    


if __name__=="__main__":
    import scatman
    import h5py
    import argparse



    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    filename=args.filename


    expparam={}
    additional={}

    f=h5py.File(filename, "r")

    nshapes=f["nshapes"][()]
    params=[]
    positions=[]

    for entry in list(f["exp_parameters"].keys()):
        expparam[entry]=f["exp_parameters"][entry][()]
        
    print(expparam)


    for ishape in range(nshapes):
        pp={}
        for entry in list(f["shape_"+str(ishape)].keys()):
            pp[entry]=f["shape_"+str(ishape)][entry][()]
        params.append(pp)
        positions.append(f["position_"+str(ishape)][()])
        print(params[-1])
        print(positions[-1])


    for entry in list(f["additional"].keys()):
        additional[entry]=f["additional"][entry][()]

    print(additional)



    #for pp in param["coefficients"]:
        #print(int(pp[0]), int(pp[1]), pp[2])

    dataexp=np.array(f["experiment"][()])
    datasim=np.array(f["simulation"][()])
    mask=np.array(f["mask"][()])
    error=np.array(f["error"][()])

    print("Errror = ", error)
    ##########################################d

    scatman.use_cpu()
    scatman.set_verbose(1)

    scatman.set_experiment(**expparam);
    mydetector = scatman.Detectors.MSFT(coordinates="detector")

    stage=scatman.Stage(**additional)
    for ishape in range(nshapes):
        stage.append(scatman.Shapes.Faces(**params[ishape]), positions[ishape])


        
    #dataexp=dataexp**additional['exponent']-additional['offset']**additional['exponent']

    #datasim=(datasim+additional['offset'])**additional['exponent']
    ########d
    dataexp[dataexp<0]=0
    exp_sum = np.sum(dataexp[mask==1])
    #############d
    #datasim = np.array(mydetector.acquire(shape))

    #datasim=np.power(datasim,additional['exponent'])
    datasim_sum=np.sum(datasim[mask==1])
    datasim=np.multiply(datasim, additional['scaling']/datasim_sum*exp_sum)
    datasim = datasim+additional['offset']
    #datasim[datasim<additional['offset']] = additional['offset']


    #dataexp[mask!=1]=np.nan
    #datasim[mask!=1]=np.nan




    dataexp[mask==0]=0
    datasim[mask==0]=0


    #import render_stage
    #render_sh.Scene([dataexp, datasim], [shapeorig, shape], wavelength_in=1., res=80)
    Scene([datasim], [stage], wavelength_in=1., dx=0.33,render_pattern=True, render_beam=True)


