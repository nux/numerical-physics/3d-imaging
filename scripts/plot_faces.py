#!/usr/bin/env python3

import numpy as np
import scatman
import h5py
import argparse
import os.path
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.image as mpimg
import matplotlib.colors as colors
import copy
import glob
import re
import os
import shutil
from PIL import Image

parser = argparse.ArgumentParser()

parser.add_argument("filenames", nargs='+')
args = parser.parse_args()
filenames = args.filenames

error_metric=1.
error_exp=1.

counts=1
for filename in filenames:
    print(counts, filename)
    counts+=1
    basename=os.path.basename(filename)
    print(basename)
    codename = re.search('fit_(.*).h5', basename).group(1)
    print(codename)
    outdir=os.path.dirname(os.path.realpath(filename))
    print(outdir)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    

    f=h5py.File(filename, "r")

    expparam={}
    additional={}
    nshapes=f["nshapes"][()]
    params=[]
    positions=[]

    for entry in list(f["exp_parameters"].keys()):
        expparam[entry]=f["exp_parameters"][entry][()]
        
    print(expparam)


    for ishape in range(nshapes):
        pp={}
        for entry in list(f["shape_"+str(ishape)].keys()):
            pp[entry]=f["shape_"+str(ishape)][entry][()]
        params.append(pp)
        positions.append(f["position_"+str(ishape)][()])
        print(params[-1])
        print("Radii:", np.sum(params[-1]["points"]**2,axis=1)**0.5)
        
        print(positions[-1])


    for entry in list(f["additional"].keys()):
        additional[entry]=f["additional"][entry][()]

    print(additional)



    dataexp=np.array(f["experiment"][()])
    datasim=np.array(f["simulation"][()])
    mask=np.array(f["mask"][()])
    error=np.array(f["error"][()][0])

    print("Error = ", error)
    ##########################################d

    scatman.use_cpu()
    scatman.set_verbose(1)

    scatman.set_experiment(**expparam);
    mydetector = scatman.Detectors.MSFT(coordinates="detector")

    stage=scatman.Stage(**additional)
    for ishape in range(nshapes):
        stage.append(scatman.Shapes.Faces(**params[ishape]), positions[ishape])
    ########d

    
    
    dataexp[dataexp<0]=0
    exp_sum = np.sum(dataexp[mask==1])
    #############d
    datasim_sum=np.sum(datasim[mask==1])
    datasim=datasim/datasim_sum * exp_sum * additional['scaling']
    datasim = datasim+additional['offset']

    dataexp[mask!=1]=0
    datasim[mask!=1]=0



    fig = plt.figure(figsize=(9,12))
    plt.tight_layout()
    ax = []
    #fig = plt.figure(constrained_layout=False, figsize=(16,8))
    spec = fig.add_gridspec(4, 3)


    ax=[ fig.add_subplot(spec[0, 0])]
    ax.append(fig.add_subplot(spec[0, 1]))
    ax.append(fig.add_subplot(spec[0, 2]))

    for aa in ax:
        aa.set_axis_off()


    maxval = np.amax(dataexp[mask==1])**error_exp
    minval = (1e-4)**error_exp * maxval

    dataexp = dataexp**error_exp
    datasim = datasim**error_exp


    ax[0].imshow(dataexp, origin="lower",norm=colors.LogNorm(vmin=minval, vmax=maxval))
    ax[1].imshow(datasim, origin="lower",norm=colors.LogNorm(vmin=minval, vmax=maxval))
    ax[2].imshow(np.abs(datasim-dataexp), origin="lower", norm=colors.LogNorm(vmin=minval, vmax=maxval))


    ax[0].set_title("ID: "+codename, fontsize=10)
    ax[1].set_title("Retrieved pattern", fontsize=10)
    ax[2].set_title("Error: {:.4e}".format(error), fontsize=10)

    ###############################################d
    dx=0.33
    
    data =stage.get(dx=dx)
    data = np.array(data)



    dataxy = np.sum(data, axis=0)
    dataxz = np.sum(data, axis=1)
    datayz = np.swapaxes(np.sum(data, axis=2), 0,1)

    maxdim = np.amax(data.shape)
    
    zeros = np.zeros([maxdim,maxdim])
    zeros[maxdim//2-dataxy.shape[0]//2:maxdim//2-dataxy.shape[0]//2+dataxy.shape[0], maxdim//2-dataxy.shape[1]//2:maxdim//2-dataxy.shape[1]//2+dataxy.shape[1]] = dataxy
    dataxy=zeros
    
    
    zeros = np.zeros([maxdim,maxdim])
    zeros[maxdim//2-dataxz.shape[0]//2:maxdim//2-dataxz.shape[0]//2+dataxz.shape[0], maxdim//2-dataxz.shape[1]//2:maxdim//2-dataxz.shape[1]//2+dataxz.shape[1]] = dataxz
    dataxz=zeros
    
    zeros = np.zeros([maxdim,maxdim])
    zeros[maxdim//2-datayz.shape[0]//2:maxdim//2-datayz.shape[0]//2+datayz.shape[0], maxdim//2-datayz.shape[1]//2:maxdim//2-datayz.shape[1]//2+datayz.shape[1]] = datayz
    datayz=zeros    

    gridsize=maxdim*dx

    
    extent=[-gridsize/2 , gridsize/2 , -gridsize/2 , gridsize/2 ]

    plt.tight_layout()

    ax3 = []

    ax3=[ fig.add_subplot(spec[1, 0])]
    ax3.append(fig.add_subplot(spec[1, 1]))
    ax3.append(fig.add_subplot(spec[1, 2]))

    for ax in ax3:
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)

    ax3[0].imshow(dataxy, extent=extent, origin="lower", cmap="pink_r")
    ax3[1].imshow(dataxz, extent=extent, origin="lower", cmap="pink_r")
    ax3[2].imshow(datayz, extent=extent, origin="lower", cmap="pink_r")

    ax3[0].grid(color='gray', linestyle=':', linewidth=0.5)
    ax3[1].grid(color='gray', linestyle=':', linewidth=0.5)
    ax3[2].grid(color='gray', linestyle=':', linewidth=0.5)

    ax3[0].set_ylabel(r"$\lambda$")
    ax3[0].set_xlabel("XY projection")
    ax3[1].set_xlabel("XZ projection")
    ax3[2].set_xlabel("ZY projection")


    x_ticks = np.arange(0, gridsize/2, 5)
    x_ticks2 = np.arange(0, -gridsize/2, -5)
    x_ticks2=x_ticks2[1:]

    x_ticks=np.concatenate([x_ticks2, x_ticks])

    ax3[0].set_xticks(x_ticks)
    ax3[1].set_xticks(x_ticks)
    ax3[2].set_xticks(x_ticks)

    ax3[0].set_xticklabels([])
    ax3[1].set_xticklabels([])
    ax3[2].set_xticklabels([])

    ax3[0].set_yticks(x_ticks)
    ax3[1].set_yticks(x_ticks)
    ax3[2].set_yticks(x_ticks)
    ax3[1].set_yticklabels([])
    ax3[2].set_yticklabels([])


    from xvfbwrapper import Xvfb
    vdisplay = Xvfb(width=1920, height=1080)
    vdisplay.start()

    import render_faces
    dist=2.
    views=[
           [0,0,-dist],
           [0,0,dist],
           [0,-dist,-1e-3],
           [0,dist,-1e-3],
           [dist,0,0],
           [-dist,0,0]]
    render_faces.Scene([np.copy(datasim)], [stage], wavelength_in=1., dx=dx, screenshot_views=views)
    
    
    screenshots=[]
    for i in range(1,7):
        screenshots.append(Image.open("screenshot_00"+str(i)+".png"))
        os.remove("screenshot_00"+str(i)+".png")
    



    axrend=[ fig.add_subplot(spec[2, 0])]
    axrend.append(fig.add_subplot(spec[3, 0]))
    axrend.append(fig.add_subplot(spec[2, 1]))
    axrend.append(fig.add_subplot(spec[3, 1]))
    axrend.append(fig.add_subplot(spec[2, 2]))
    axrend.append(fig.add_subplot(spec[3, 2]))
    
    for aa in axrend:
        aa.set_axis_off()
        
      
    for i in range(0,6):
        axrend[i].imshow(screenshots[i])
        
    plt.tight_layout()
    
    #fig.suptitle("ID: "+codename+"\n", fontsize=14, )
    
    #plt.figure(fig)
    plt.tight_layout()
    plt.savefig(os.path.join(outdir, "plot.png"), dpi=150, bbox_inches='tight' )
    plt.show()
    plt.close('all')
    vdisplay.stop()
    #############d
    

