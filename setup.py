from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
import sys
import setuptools



__version__ = '0.1'

setup(name='3D imaging',
version='0.1',
description='3D CDI fitting toolset',
url='#',
author='Alessandro Colombo',
author_email='alcolombo@phys.ethz.ch',
license='MIT',
packages=['imaging3Dtools'],
scripts=['scripts/plot_faces.py', 'scripts/render_faces.py',  'scripts/fit_faces.py'],
zip_safe=False,
install_requires=[
   'numpy',
   'argparse',
   'h5py',
   'pillow',
   'matplotlib',
   'scipy',
   'vtk'
]
#,
#ext_modules=ext_modules,
#cmdclass={'build_ext': BuildExt}
#setup_requires=['pybind11>=2.5.0'],

)
