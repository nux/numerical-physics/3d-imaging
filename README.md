# 3D Imaging

For accessing the documentation and the most up to date code, please visit the gitlab repository at https://gitlab.ethz.ch/nux/numerical-physics/3d-imaging

To run the software, the pyScatman python package is necessary. Please visit https://gitlab.ethz.ch/nux/numerical-physics/pyscatman

## Quick start

The software can be installed via pip. From the project folder, type the following command:

python3 -m pip install . 

After this command, three scripts are installed in your system:
* fit_faces.py : the fitting software. Run with option --help for details
* plot_faces.py : plot the fitting result.
* render_faces.py : visualize a 3d rendering of the reconstruction 

