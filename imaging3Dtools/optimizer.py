import numpy as np
import random
import copy
import scatman
import h5py
import os
from multiprocessing import Pool, Queue
import scipy 
import scipy.stats
import time

from . import target_functions 
from . import neldermead as neldermead

from .setup_data import read_data


######d helper functions




def extract(size, coeff):
    
    return int(np.random.random()**coeff * size)

def init_worker(q):
    seed=q.get()
    seed = int(seed)%(2**32)
    np.random.seed(seed)
    import os
    print ("Process {0} init with seed {1}".format(os.getpid(),seed))
    
######################




class Optimizer():
    
    def __init__(self, workers=1, seed=10, tag="", save_steps=False, profile=False):
        self.workers=workers
        self.chunksize=32
        seed = int(seed)%(2**32)
        np.random.seed(seed)
        seeds= Queue()
        for iw in range(self.workers):
            seeds.put(seed*(2+iw))
            
            
        self.save_steps=save_steps
        self.steps_folder="./output"
        
        self.mean_step=0
        self.pool = Pool(processes=workers, initializer=init_worker,initargs=(seeds,)) 
    
        self.tag=""
        if tag=="":
            self.tag="temp"
        else:
            self.tag=tag
        self.best_filename="fit_result_"+tag+".h5"
        self.avg_filename="fit_avg_"+tag+".h5"
        
        self.t0=0
        self.t1=0
        self.profile=profile
        if self.profile:
            import cProfile
            self.pr = cProfile.Profile()
    
    
    def set_population(self,population, bounds, popsize=-1):
        self.input_population=population
        self.bounds=bounds
        self.popsize=popsize
    
    
    def read_data(self, dataname, center, cutsize, resolution, detdistance, pixelsize, satlevel, minlevel = 0):
        self.resolution=resolution
        self.exp_pattern, self.mask, self.angle = read_data(dataname, center, cutsize, resolution, detdistance, pixelsize, satlevel, minlevel )
          
        
        
    def set_data(self, error_exp, error_metric):
        self.error_metric=error_metric
        self.error_exp = error_exp
        self.exp_norm = np.sum(self.exp_pattern[self.mask==1])
        self.err_pattern = self.exp_pattern**error_exp
        self.err_norm = np.sum(self.err_pattern[self.mask==1])      
        
        
    def set_simulation(self, sim_resolution, gpuids, threads):
        
        self.sim_resolution=sim_resolution
        
        scatman.use_gpu()
        #scatman.use_cpu()


        scatman.set_gpu_ids(gpuids)
        
        totthreads = len(gpuids)*threads
        if totthreads==0:
            totthreads=threads

        scatman.set_cpu_threads(totthreads)


        self.expparam={ "wavelength" : 1, "angle" : self.angle, "resolution" : self.sim_resolution, "rescale" : self.resolution}

        scatman.set_fourier_oversampling(2)

        scatman.set_experiment(**self.expparam);
        scatman.info()

        self.mydetector = scatman.Detectors.MSFT(coordinates='detector')

        scatman.set_verbose(0)



    def set_bounds(self, bounds):
        self.bounds = bounds


    def set_genetic_parameters(self, generations, crossexp,crossbal, crossdiff):
        self.generations=generations
        self.crossexp=crossexp
        self.crossbal=crossbal
        self.crossdiff=crossdiff
    
    def set_simplex_parameters(self, maxfev, simplex_step, simplex_pop_step, nopt, parallel=True, multi=False, nopt_halfsteps=-1, nopt_min=1, maxopt=20 ):
        self.maxfev=maxfev
        self.simplex_step=simplex_step
        self.simplex_pop_step=simplex_pop_step
        self.nopt=nopt
        self.nopt_init=nopt
        self.parallel_nm=parallel
        self.nm_multi=multi

        self.nopt_min=nopt_min
        self.maxoptimize=maxopt

        self.nopt_halfcoeff=1.
        if nopt_halfsteps>0:
            self.nopt_halfcoeff=1./(2.**(1./nopt_halfsteps))

    def update_nopt(self):
        if self.nopt_halfcoeff<1 and self.nopt>self.nopt_min:
            self.nopt=int(round(self.nopt_init*self.nopt_halfcoeff**self.istep))
            
            


    def save_h5(self, filename, X):
        f=h5py.File(filename, "w")

        nshapes = len(X['shape'])
        additional = X['additional']

        f.create_dataset("nshapes", data=nshapes)

        
        for key in additional:
            f.create_dataset("additional/"+str(key), data=additional[key])
        
        
        for ishape in range(nshapes):
            for key in X['shape'][ishape]:
                f.create_dataset("shape_"+str(ishape)+"/"+str(key), data=X['shape'][ishape][key])
            #for key in X['refractive_index']:
                #f.create_dataset("shape_"+str(ishape)+"/"+str(key), data=X['refractive_index'][key])

                
            f.create_dataset("position_"+str(ishape), data=X['position'][ishape])
            
        for key in self.expparam:
            f.create_dataset("exp_parameters/"+str(key), data=self.expparam[key])
            

        f.create_dataset("experiment", data=self.exp_pattern)

        pattern=np.array(self.getpatterns([X])[0])

        f.create_dataset("simulation", data=pattern)

        f.create_dataset("mask", data=self.mask)

        f.create_dataset("error", data=self.target([X]))

        f.close()


    #def target(self, X):
        ##shapes=[]

        #stages=[]
        
        #for xx in X:
            #stage=scatman.Stage(**xx['additional'])
            #shapes=[]
            #for ishape in range(len(xx['shape'])):
                #stage.append(scatman.Shapes.Faces(**xx['shape'][ishape]), xx['position'][ishape])
            #stages.append(stage)
        #errors =  self.mydetector.compare(stages, self.err_pattern, self.mask, self.err_norm, self.exp_norm, self.error_exp, self.error_metric) 
        
        #return np.asarray(errors)

    def scatman_sim(self, stages):
        patterns = self.mydetector.acquire(stages)
        return patterns
    
    def target(self, X):
        #shapes=[]

        stages=[]
        
        for xx in X:
            stage=scatman.Stage(**xx['additional'])
            shapes=[]
            for ishape in range(len(xx['shape'])):
                #stage.append(scatman.Shapes.Faces(**xx['shape'][ishape],**xx['refractive_index']), xx['position'][ishape])
                stage.append(scatman.Shapes.Faces(**xx['shape'][ishape]), xx['position'][ishape])

            stages.append(stage)
            
        patterns =  self.scatman_sim(stages)
        
        errors=[]
        for ip in range(len(patterns)):
            pp=np.asarray(patterns[ip])
            psum=np.sum(pp[self.mask==1])
            pp= pp*X[ip]['additional']['scaling']/psum*self.exp_norm+X[ip]['additional']['offset']
            error=np.sum((np.abs(self.err_pattern[self.mask==1] -pp[self.mask==1]**self.error_exp))**self.error_metric)
            error=error**(1./self.error_metric)/self.err_norm
            errors.append(error*target_functions.get_penalty(xx))
                
        return np.asarray(errors)



    def getpatterns(self, X):

        stages=[]
        
        for xx in X:
            stage=scatman.Stage(**xx['additional'])
            shapes=[]
            for ishape in range(len(xx['shape'])):
                #stage.append(scatman.Shapes.Faces(**xx['shape'][ishape],**xx['refractive_index']), xx['position'][ishape])
                stage.append(scatman.Shapes.Faces(**xx['shape'][ishape]), xx['position'][ishape])

            stages.append(stage)
        patterns =  self.mydetector.acquire(stages)
        return patterns






    def callback(self):
        
        print(self.istep, 
              '\tBest: {:.3e}'.format(self.overall_best_err), 
              'Current: {:.3e}'.format(self.best_err), 
              'Pop: {:.3e}'.format(np.amin(self.errors))+' <{:.3e}'.format(np.mean(self.errors)) + ' +- {:.3e}>'.format(np.std(self.errors)), 
              'Avg: {:.3e}'.format(self.average_err), 
              'Repl.:{:.1f}'.format(self.replace*100.), 
              'Opt.:{:d} ({:d})[{:.2e}]'.format(int(np.sum(np.asarray(self.optimized)>0)), self.nopt, self.mean_step ),
              'Pen.:{:.3e}'.format(target_functions.get_penalty(self.overall_best)),
              'Time:{:.1f}'.format(self.t1-self.t0)
              )
        
        
        self.save_h5(".fit_result_temp.h5", self.overall_best)
        os.replace(".fit_result_temp.h5", self.best_filename) 
        self.save_h5(".fit_avg_temp.h5", self.average)
        os.replace(".fit_avg_temp.h5", self.avg_filename) 
        
        if self.save_steps:
            #tag=str(self.overall_best["parameters"]["points"].shape[0])+"_"+str(self.istep)
            tag=str(self.istep).zfill(4)
            self.save_h5(self.steps_folder+"/fit_result_"+tag+".h5", self.overall_best)
            self.save_h5(self.steps_folder+"/fit_avg_"+tag+".h5", self.average)
    
  

    def self_improvement(self):
        nvars = target_functions.encode(self.population[0], self.bounds).shape[0]
        sortind=np.argsort(self.errors)
        Xmin, Xmax=target_functions.encode_bounds(self.bounds)
        pointspershape=[xx["points"].shape[0] for xx in self.bounds['shape']]
        totalpoints = np.sum(pointspershape)
        
        Xrange=(Xmax-Xmin)
        totopt=0

        simplices = []
        origerrors = []
        origbests = []
        optindexes = []
        x0 = []
        
        totmagnitudes = []
        
        for iopt in range(self.popsize):
            if totopt==self.nopt:
                break
            if self.optimized[sortind[iopt]]<self.maxoptimize:# or iopt==0:
                totopt+=1
                
                origbest=copy.deepcopy(self.population[sortind[iopt]])


                
                popind = sortind[iopt]
                while popind == sortind[iopt]:
                    popind = sortind[extract(self.popsize, self.crossexp)]
                    
                
                simplexpop = copy.deepcopy(self.population[popind])
                
                target_functions.reorder([origbest]+[simplexpop], copy.deepcopy(origbest))

                
                Xpop = target_functions.encode(simplexpop, self.bounds)
                X=target_functions.encode(origbest, self.bounds)    
                
                diff = Xpop-X
                
                
                diff = diff/Xrange
                
                diff=np.sort(diff)[:diff.shape[0]//2]
                #magnitude = np.sum(diff**2)**0.5
                magnitude = nvars**0.5 * np.sum(np.abs(diff))/diff.shape[0]
                                
                init_simplex=np.zeros([nvars+1, nvars])
                init_simplex[0,:]=X
                
                localstepsize=np.random.rand()*(self.simplex_step[1]-self.simplex_step[0]) + self.simplex_step[0]
                localpopstepsize=np.random.rand()*(self.simplex_pop_step[1]-self.simplex_pop_step[0]) + self.simplex_pop_step[0]

                totmagnitude = localstepsize + magnitude*localpopstepsize

                totmagnitudes.append(totmagnitude)
                
                directions = scipy.stats.ortho_group.rvs(nvars)
                
                for i in range(0,nvars):          
                    init_simplex[i+1,:] = np.copy(X) + directions[i]*Xrange*totmagnitude

                
                simplices.append(init_simplex)
                optindexes.append(sortind[iopt])
                origbests.append(origbest)
                x0.append(X)                

        self.mean_step=np.mean(totmagnitudes)

        if self.nm_multi:
            res=neldermead.minimize_neldermead_bounds_multi(func = self.target, x0=x0, bounds=[Xmin, Xmax], rbounds=self.bounds["_radius"], totpoints=totalpoints,  args = (), maxfev=self.maxfev*nvars, adaptive=True, initial_simplices=np.array(simplices), xatol=1e-8, decode_func=target_functions.decode, decode_args=(origbests,self.bounds), pool=self.pool)
            
        else: 
            res=neldermead.minimize_neldermead_bounds(func = self.target, x0=x0, bounds=[Xmin, Xmax], rbounds=self.bounds["_radius"], totpoints=totalpoints,  args = (), maxfev=self.maxfev*nvars, adaptive=True, initial_simplices=np.array(simplices), xatol=1e-8, decode_func=target_functions.decode, decode_args=(origbests,self.bounds))


        for iopt in range(totopt):
            newbest=target_functions.decode(res.x[iopt], copy.deepcopy(self.population[optindexes[iopt]]), self.bounds)
            newerr=self.target([newbest])[0]
            self.population[optindexes[iopt]]=copy.deepcopy(newbest)  
            self.opterrors[optindexes[iopt]]=copy.deepcopy(newerr) 
            self.optimized[optindexes[iopt]]+=1


    def crossover(self):
        
        
    
        crossargs = []
        optindexes = []
        opterrors_local=[]
        
        totopt = 0
        
        for ipop in range(self.popsize):
            if self.optimized[ipop]>0:
                optindexes.append(ipop) 
                opterrors_local.append(self.opterrors[ipop])
                totopt+=1
                
            else:
                optindexes.append(ipop) 
                opterrors_local.append(self.errors[ipop])
        
        if totopt<self.nopt:
            totopt=len(optindexes)
        
        
        sortind=np.argsort(opterrors_local)
        
        p0ind=np.arange(0,self.popsize)
        '''
        tt=np.random.choice([True,False], size=self.popsize, p=[0.1,0.9])
        
        parray=p0ind[tt]
        perm=np.random.permutation(parray)


        p0ind[tt]=perm
        '''
        for ipop in range(self.popsize):
            parentsind=[0,0,0,0]
            
            while(len(parentsind) != len(np.unique(parentsind))):         
                parentsind=[
                    p0ind[ipop],
                    optindexes[sortind[np.random.randint(0,self.nopt)]],
                    optindexes[sortind[extract(len(sortind), self.crossexp)]],
                    optindexes[sortind[extract(len(sortind), self.crossexp)]]
                            ]       
                #parentsind=[
                    #p0ind[ipop],
                    #optindexes[sortind[np.random.randint(0,self.nopt)]],
                    #optindexes[sortind[extract(totopt, self.crossexp)]],
                    #optindexes[sortind[extract(totopt, self.crossexp)]]
                            #]      

            
            parents=[self.population[parentsind[0]],
                     self.population[parentsind[1]],
                     self.population[parentsind[2]],
                     self.population[parentsind[3]]
                            ]
            crossargs.append((parents, self.bounds, self.crossbal, np.random.rand()*(self.crossdiff[1]-self.crossdiff[0]) + self.crossdiff[0]))
            

            
        self.newpop = []
        self.newpop = self.pool.starmap(target_functions.get_element, crossargs, chunksize=self.chunksize)


    def selection(self):
        counter=0.
        for ipop in range(self.popsize):
            if self.newerrors[ipop]<self.errors[ipop]:
                counter+=1.
                self.errors[ipop]=self.newerrors[ipop]
                self.population[ipop]=copy.deepcopy(self.newpop[ipop])
                self.optimized[ipop]=0
                self.opterrors[ipop]=-1

        self.replace= counter/float(self.popsize)

    def get_average(self):
        
        optelements=[]
        
        sortind=np.argsort(self.errors)

        for ipop in range(0,self.nopt):
            optelements.append(copy.deepcopy(self.population[sortind[ipop]]))
        
        target_functions.reorder(optelements, optelements[0])

        
        aver=copy.deepcopy(optelements[0])
        nshapes=len(aver['shape'])
        for ipop in range(1,self.nopt):
            for ishape in range(nshapes):
                for key in  aver['shape'][ishape]:
                    aver['shape'][ishape][key]=aver['shape'][ishape][key] + optelements[ipop]['shape'][ishape][key]
                aver['position'][ishape]=aver['position'][ishape]+optelements[ipop]['position'][ishape]
            
            for key in  aver['additional']:
                aver['additional'][key]=aver['additional'][key] + optelements[ipop]['additional'][key]


        for ishape in range(nshapes):
            for key in  aver['shape'][ishape]:
                aver['shape'][ishape][key]=aver['shape'][ishape][key]/self.nopt
            aver['position'][ishape]=aver['position'][ishape]/self.nopt
        
        for key in  aver['additional']:
            aver['additional'][key]=aver['additional'][key]/self.nopt
                
        self.average=aver
        self.average_err=self.target([self.average])[0]


    def get_best(self):
        
        optindexes=[]
        opterrors_local=[]
        for ipop in range(self.popsize):
            if self.optimized[ipop]>0:
                optindexes.append(ipop) 
                opterrors_local.append(self.opterrors[ipop])
            else:
                optindexes.append(ipop) 
                opterrors_local.append(self.errors[ipop])
        
        self.best=self.population[np.argmin(opterrors_local)]
        self.best_err=self.target([self.best])[0]
        
        if self.best_err<self.overall_best_err:
            self.overall_best= copy.deepcopy(self.best)            
            self.overall_best_err = np.copy(self.best_err)
            
            
    def optimize(self):

        if self.popsize<0 or self.popsize>= len(self.input_population):
            self.popsize=len(self.input_population)

        print("Evaluation of input population")
        input_errors=self.target(self.input_population)
        sortind=np.argsort(input_errors)
        
        self.population = []
        self.errors = []
        
        for ipop in range(self.popsize):    
            self.population.append(self.input_population[sortind[ipop]])
            self.errors.append(input_errors[sortind[ipop]])
        
        self.newpop = []
        self.newerrors= []
        self.best=self.population[np.argmin(self.errors)]
        self.best_err=np.amin(self.errors)
        self.overall_best=copy.deepcopy(self.population[np.argmin(self.errors)])
        self.overall_best_err = np.amin(self.errors)
        self.get_average()
        
        self.istep=0
        self.replace=0
        self.optimized=[0]*self.popsize
        self.opterrors=[-1]*self.popsize
        
        self.callback()

        print(self.popsize)
        
        print("Start main loop")

    
        if self.profile:
            self.pr.enable()
    
        for self.istep in range(1,self.generations+1):        
            
            self.t0=time.time()
            
            self.crossover()

            self.newerrors=self.target(self.newpop)
                    
            self.selection()
            
            self.best=self.population[np.argmin(self.errors)]
            
            self.self_improvement()

            self.get_average()
            
            self.get_best()

            self.t1=time.time()
            self.callback()
            
            self.update_nopt()


        if self.profile:
            self.pr.disable()
            self.pr.dump_stats("profile")
        
        
















