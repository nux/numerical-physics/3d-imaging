import numpy as np
import scatman
import copy



###############################################d

def get_max_radius(X):
    val=0
    for ishape in range(len(X['shape'])):
        radii = np.std(X["shape"][ishape]["points"]**2, axis=1)**0.5
        val += np.std(radii)
    return val

def get_penalty(X):
    return 1.



def encode(element, bounds):
    X=np.zeros([0])
    
    nshapes=len(bounds['shape'])
    for ishape in range(nshapes):
        X = np.append(X, element["shape"][ishape]["points"].flatten())

    for ishape in range(nshapes):    
        for key in bounds["shape"][ishape]:
            if key !='points' and bounds["shape"][ishape][key][1]!=bounds["shape"][ishape][key][0]:
                X=np.append(X,np.copy(element["shape"][ishape][key]))
        X=np.append(X,np.copy(element["position"][ishape].flatten()))
        
    for key in bounds["additional"]:
        if bounds["additional"][key][1]!=bounds["additional"][key][0]:
            X=np.append(X,np.copy(element["additional"][key]))    

   
    return X

def encode_bounds(bounds):
    Xmax=np.zeros([0])
    Xmin=np.zeros([0])
    
    nshapes=len(bounds['shape'])
    for ishape in range(nshapes):
        Xmax = np.append(Xmax,bounds["shape"][ishape]["points"][:,:,1].flatten())
        Xmin = np.append(Xmin,bounds["shape"][ishape]["points"][:,:,0].flatten())

    for ishape in range(nshapes):
        for key in bounds["shape"][ishape]:
            if key !='points' and bounds["shape"][ishape][key][1]!=bounds["shape"][ishape][key][0]:
                Xmax=np.append(Xmax,np.copy(bounds["shape"][ishape][key][1]))
                Xmin=np.append(Xmin,np.copy(bounds["shape"][ishape][key][0]))

        Xmax=np.append(Xmax,np.copy(bounds["position"][ishape][:,1].flatten()))
        Xmin=np.append(Xmin,np.copy(bounds["position"][ishape][:,0].flatten()))
        
    for key in bounds["additional"]:
        if bounds["additional"][key][1]!=bounds["additional"][key][0]:
            Xmax=np.append(Xmax,np.copy(bounds["additional"][key][1]))    
            Xmin=np.append(Xmin,np.copy(bounds["additional"][key][0]))   
            

    return Xmin, Xmax




def decode(X, inelement, bounds):
##################d
    element=inelement

    counter=0
    nshapes=len(bounds['shape'])
    for ishape in range(nshapes):
        points_shape=element["shape"][ishape]["points"].shape
        size=element["shape"][ishape]["points"].flatten().shape[0]
        element["shape"][ishape]["points"]=X[counter:counter+size].reshape(points_shape)
        counter+=size

    for ishape in range(nshapes):    
        for key in bounds["shape"][ishape]:
            if key !='points' and bounds["shape"][ishape][key][1]!=bounds["shape"][ishape][key][0]:
                element["shape"][ishape][key] = X[counter]
                counter+=1
        
        element["position"][ishape]=X[counter:counter+3]
        counter+=3
        
    for key in bounds["additional"]:
        if bounds["additional"][key][1]!=bounds["additional"][key][0]:
            element["additional"][key]=X[counter]
            counter+=1

            
    return element

def get_averrad(data):
    refradii =  np.sum(data**2, axis=1)**0.5
    return np.mean(np.sort(refradii)[:refradii.shape[0]//2])

    
def reorder_element(pointarray, refpointarray):
    
    
    npoints = refpointarray.shape[0]
    
    refradii =  np.sum(refpointarray**2, axis=1)**0.5
    pointradii=  np.sum(refpointarray**2, axis=1)**0.5
    
    refscale = np.mean(np.sort(refradii)[:refradii.shape[0]//2])
    pointscale = np.mean(np.sort(pointradii)[:pointradii.shape[0]//2])
    
    scaledrefpointarray=refpointarray/refscale
    scaledpointarray=pointarray/pointscale
    

    
    for ipoint in range(npoints-1):
        dists = scaledpointarray[ipoint:,:] - scaledrefpointarray[ipoint,:]
        dists = np.sum(dists**2, axis=1)
        minind=np.argmin(dists)
        scaledpointarray[[ipoint, ipoint+minind],:]=scaledpointarray[[ipoint+minind, ipoint],:]  
        
    

    pointarray[:]=scaledpointarray[:]*pointscale

def recenter(population):
    
    for element in population:
        elpos = np.array(element['position'])
        radii = np.array([get_averrad(x['points']) for x in element['shape']])
        
        center= np.sum(elpos*radii[:,np.newaxis], axis=0)/np.sum(radii)
        
        for ipos in range(len(element['position'])):
            element['position'][ipos]=element['position'][ipos]-center
              

def reorder_shapes(population, ref):

    recenter(population)

    nshapes=len(ref['shape'])
    radii = np.array([get_averrad(x['points'])for x in ref['shape']])
    
    shapeindexes=np.argsort(radii)[::-1]
    
    refpos=np.copy(np.array(ref['position'])[shapeindexes,:])
    
    for element in population:
        elpos = np.copy(np.array(element['position']))
        
        for ishape in range(nshapes-1):
            dists = elpos[ishape:,:] - refpos[ishape,:]
            dists = np.sum(dists**2, axis=1)
            minind=np.argmin(dists)
            elpos[[ishape, ishape+minind],:]=elpos[[ishape+minind, ishape],:]  
            element['shape'][ishape], element['shape'][ishape+minind] = element['shape'][ishape+minind], element['shape'][ishape]
            element['position'][ishape], element['position'][ishape+minind] = element['position'][ishape+minind], element['position'][ishape]
    
    return shapeindexes[0]



def reorder(population, ref):
    
    iref=reorder_shapes(population, ref)
    
    nshapes=len(ref['shape'])
    
    for ishape in range(nshapes):
        
        npoints = ref["shape"][ishape]["points"].shape[0]

        radii = np.sum(ref["shape"][ishape]["points"]**2, axis=1)
        ind = np.argsort(radii)
        
       

        ref["shape"][ishape]["points"]=ref["shape"][ishape]["points"][ind,:]
        
        
        for i in range(0, len(population)):
            reorder_element(population[i]["shape"][ishape]["points"], ref["shape"][ishape]["points"])


def get_shape(parents, bounds, rbounds, crossbal, crossdiff):
    pointsshape = parents[0]["points"].shape
        
    lowerpoints=bounds["points"][:,:,0]
    upperpoints= bounds["points"][:,:,1]

    
    averradii = []
    scaledpoints = []
    
    for i in range(4):
        averradius = np.mean(np.sum(parents[i]["points"]**2, axis=1)[:pointsshape[0]//2]**0.5)
        scaled = parents[i]["points"]/averradius
        
        averradii.append(averradius)
        scaledpoints.append(scaled)

    radii = []
    for i in range(4):
        radii.append(np.sum(scaledpoints[i]**2, axis=1)**0.5)
    

    element={}
    for key in bounds:
        if key =='points':
        
            crossmap = np.random.choice([True, False], size=pointsshape[0], p=[crossbal, 1.-crossbal])
            coeff = np.copy(scaledpoints[0])
            coeff[crossmap==True,:] = scaledpoints[1][crossmap==True,:] + crossdiff*(scaledpoints[2][crossmap==True,:] -scaledpoints[3][crossmap==True,:] )
            
            temprad=np.copy(radii[0])
            temprad[crossmap==True] = radii[1][crossmap==True] + crossdiff*(radii[2][crossmap==True] -radii[3][crossmap==True] )
            
            for j in range(coeff.shape[0]):
                coeff[j,:]=coeff[j,:]/np.sum(coeff[j,:]**2)**0.5*temprad[j]
                #coeff=coeff*temprad

            newaver = averradii[0]
            if np.random.rand()<crossbal:
                newaver = averradii[1] + crossdiff*(averradii[2]-averradii[3])
            
            
            coeff=coeff*newaver

        
            newradii = np.sum(coeff**2, axis=1)**0.5
            newradii_proj = np.clip(newradii, rbounds[0], rbounds[1])
            newradii_refl = 2.*newradii_proj-newradii
            newradii = np.clip(newradii_refl, rbounds[0], rbounds[1])
            for j in range(coeff.shape[0]):
                coeff[j,:]=coeff[j,:]/(np.sum(coeff[j,:]**2)**0.5)*newradii[j]

            element[key]=coeff



            
        else:
            if np.random.rand()<crossbal:
                element[key]=parents[1][key]+crossdiff*(parents[2][key]-parents[3][key])
            else:
                element[key]=np.copy(parents[0][key])
            
            proj = np.clip(element[key],bounds[key][0], bounds[key][1])
            val = 2.*proj-element[key]
            element[key]=np.clip(val,bounds[key][0], bounds[key][1])
    return element    



def get_element(parents_in, bounds, crossbal, crossdiff):
    parents=copy.deepcopy(parents_in)


    reorder(parents, copy.deepcopy(parents[1]))

    
    element = copy.deepcopy(parents[0])

    nshapes=len(bounds['shape'])

    for ishape in range(nshapes):
        element['shape'][ishape]=get_shape(
            [parents[0]['shape'][ishape],
             parents[1]['shape'][ishape],
             parents[2]['shape'][ishape],
             parents[3]['shape'][ishape]],
            bounds['shape'][ishape], bounds["_radius"], crossbal, crossdiff
            )

 
        crossmap = np.random.choice([True, False], size=3, p=[crossbal, 1.-crossbal])
        coeff = np.copy(parents[0]['position'][ishape])
        coeff[crossmap==True] =parents[1]['position'][ishape][crossmap==True] + crossdiff*(parents[2]['position'][ishape][crossmap==True] -parents[3]['position'][ishape][crossmap==True] )
        
 
        element['position'][ishape]=coeff

        proj = np.clip(element['position'][ishape],bounds['position'][ishape][:,0], bounds['position'][ishape][:,1])
        val = 2.*proj-element['position'][ishape]
        element['position'][ishape]=np.clip(val,bounds['position'][ishape][:,0], bounds['position'][ishape][:,1])
            
    for key in bounds["additional"]:
        if np.random.rand()<crossbal:
            element["additional"][key]=parents[1]["additional"][key]+crossdiff*(parents[2]["additional"][key]-parents[3]["additional"][key])
        else:
            element["additional"][key]=np.copy(parents[0]["additional"][key])
        proj = np.clip(element["additional"][key],bounds["additional"][key][0], bounds["additional"][key][1])
        val = 2.*proj-element["additional"][key]
        element["additional"][key]=np.clip(val,bounds["additional"][key][0], bounds["additional"][key][1])

            


    recenter([element])

    return element
    


