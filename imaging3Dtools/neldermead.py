# The Nelder-Mead Algorithom from scipy, edited to allow optimizing from multiple guesses in parallel
# (see nelder_mead_combined). The original algorithm is still contained, see nelder_mead_single.

# Changes by Patrice Kolb, pakolb@bluewin.ch

# ******NOTICE***************
# optimize.py module by Travis E. Oliphant
#
# You may copy and use this module as you see fit with no
# guarantee implied provided you keep this notice in all copies.
# *****END NOTICE************

__docformat__ = "restructuredtext en"

import warnings
import sys
from numpy import (atleast_1d, eye, argmin, zeros, shape, squeeze,
                   asarray, sqrt, Inf, asfarray, isinf)
import numpy as np
from scipy._lib._util import getfullargspec_no_self as _getfullargspec
from scipy._lib._util import MapWrapper
from scipy.optimize._differentiable_functions import ScalarFunction, FD_METHODS
from multiprocessing import Pool, Queue, Pipe
import copy
import os

# standard status messages of optimizers
_status_message = {'success': 'Optimization terminated successfully.',
                   'maxfev': 'Maximum number of function evaluations has '
                              'been exceeded.',
                   'maxiter': 'Maximum number of iterations has been '
                              'exceeded.',
                   'pr_loss': 'Desired error not necessarily achieved due '
                              'to precision loss.',
                   'nan': 'NaN result encountered.',
                   'out_of_bounds': 'The result is outside of the provided '
                                    'bounds.'}


class OptimizeResult(dict):
    """ Represents the optimization result.

    Attributes
    ----------
    x : ndarray
        The solution of the optimization.
    success : bool
        Whether or not the optimizer exited successfully.
    status : int
        Termination status of the optimizer. Its value depends on the
        underlying solver. Refer to `message` for details.
    message : str
        Description of the cause of the termination.
    fun, jac, hess: ndarray
        Values of objective function, its Jacobian and its Hessian (if
        available). The Hessians may be approximations, see the documentation
        of the function in question.
    hess_inv : object
        Inverse of the objective function's Hessian; may be an approximation.
        Not available for all solvers. The type of this attribute may be
        either np.ndarray or scipy.sparse.linalg.LinearOperator.
    nfev, njev, nhev : int
        Number of evaluations of the objective functions and of its
        Jacobian and Hessian.
    nit : int
        Number of iterations performed by the optimizer.
    maxcv : float
        The maximum constraint violation.

    Notes
    -----
    There may be additional attributes not listed above depending of the
    specific solver. Since this class is essentially a subclass of dict
    with attribute accessors, one can see which attributes are available
    using the `keys()` method.
    """

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError as e:
            raise AttributeError(name) from e

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __repr__(self):
        if self.keys():
            m = max(map(len, list(self.keys()))) + 1
            return '\n'.join([k.rjust(m) + ': ' + repr(v)
                              for k, v in sorted(self.items())])
        else:
            return self.__class__.__name__ + "()"

    def __dir__(self):
        return list(self.keys())


def wrap_function(function, args):
    ncalls = [0]
    if function is None:
        return ncalls, None

    def function_wrapper(*wrapper_args):
        ncalls[0] += 1
        return function(*(wrapper_args + args))

    return ncalls, function_wrapper


#def wrap_function(function):
    #ncalls = [0]
    #if function is None:
        #return ncalls, None

    #def function_wrapper(*wrapper_args):
        #ncalls[0] += 1
        #return function(*(wrapper_args ))

    #return ncalls, function_wrapper







class OptItem:
    
    def __init__(self, in_bounds, in_rbounds, in_totpoints, in_initial_simplex, in_adaptive, in_decode_func, origbest, origbounds):

        self.shrink_phase=0
        self.phase = 0
        self.sim = []
        self.fsim=[]
        self.bounds = []
        self.rbounds = []
        self.totpoints = 0
        self.rho = 1
        self.chi = 2
        self.psi = 0.5
        self.sigma = 0.5
        self.adaptive = False
        self.N = 0
        self.frs=0
        self.evpoint=None
        self.xbars=[]
        self.decode = in_decode_func
        self.evpoint_decoded=origbest
        self.origbounds=origbounds
        
        
        self.bounds=in_bounds
        self.rbounds=in_rbounds
        self.totpoints=in_totpoints
        self.initial_simplex = in_initial_simplex
        self.adaptive=in_adaptive
        self.phase = int(2)
        self.shrink_phase = int(0)
        
        if self.adaptive:
            dim = float(len(self.initial_simplex[0]))
            self.rho = 1
            self.chi = 1 + 2/dim
            self.psi = 0.75 - 1/(2*dim)
            self.sigma = 1 - 1/dim
        else:
            self.rho = 1
            self.chi = 2
            self.psi = 0.5
            self.sigma = 0.5

        self.N = self.initial_simplex.shape[1]
        self.sim = np.asfarray(self.initial_simplex).copy()
        self.fsim = np.empty((self.N + 1), float)

        for n in range(self.N+1): 
            self.sim[n]=self.apply_bounds(self.sim[n])        
        

    def apply_bounds(self, data):

        coords = np.copy(data[:self.totpoints*3])
        coords = coords.reshape([self.totpoints,3])
        radii = np.sum(coords**2, axis=1)**0.5
        
        radiiclipped=np.clip(radii, self.rbounds[0], self.rbounds[1])
        coords=coords/(radii[:,np.newaxis])*radiiclipped[:,np.newaxis]
        data[:self.totpoints*3]=coords.flatten()
        data[:] =  np.clip(data, self.bounds[0], self.bounds[1])
        
        return data
    
    def get_simplex(self):
        
        Xs=[]
        for n in range(self.N+1):
            Xs.append(self.decode(self.sim[n], copy.deepcopy(self.evpoint_decoded),  self.origbounds))
            
        
        return Xs

    def get_errors(self):
        return self.fsim


    def set_errors(self, fsim_in):
        self.fsim=fsim_in

    def get_init_point(self):
        ind = np.argsort(self.fsim)
        self.fsim = np.take(self.fsim, ind, 0)
        self.sim = np.take(self.sim, ind, 0) 
        
        self.xbars = np.add.reduce(self.sim[:-1], 0) / self.N
        self.evpoint=  (1 + self.rho) * self.xbars - self.rho * self.sim[-1]
        self.evpoint = self.apply_bounds(self.evpoint)
        self.evpoint_decoded=self.decode(self.evpoint, self.evpoint_decoded,  self.origbounds)
    
        return self.evpoint_decoded
    

    def make_iteration(self, result):
        
        #import os

        #print(os.getpid(), len(self.evpoint), self.evpoint[0])
        origphase = self.phase
        if origphase == 2:
            f_r = result
            
            if f_r < self.fsim[0]: # Reflected point is the new best point
                # Expansion
                self.oldevpoint=np.copy(self.evpoint)
                self.frs = f_r # Needs to be remembered

                self.evpoint = (1 + self.rho * self.chi) * self.xbars - self.rho * self.chi * self.sim[-1]
                self.phase = 3
                
            else: # Reflected point is not the new best point, f_1 <= f_r
                if f_r < self.fsim[-2]: # f_1 <= f_r < f_n
                    self.sim[-1] = np.copy(self.evpoint)
                    self.fsim[-1] = f_r
                    self.phase = 2
                    # Iteration done
                    
                else:  # f_n <= f_r
                    if f_r < self.fsim[-1]: # f_n <= f_r < f_{n-1}
                        # Outside contraction
                        self.frs = f_r # Needs to be remembered                        
                        self.evpoint= (1 + self.psi * self.rho) * self.xbars - self.psi * self.rho * self.sim[-1]
                        self.phase = 4
                    
                    else: # f_{n+1} <= f_r
                        # Inside contraction
                        self.evpoint = (1 - self.psi) * self.xbars + self.psi * self.sim[-1]
                        self.phase = 5
                    
        elif origphase == 3:
            f_e = result
            if f_e < self.frs: # Expanded point is even better
                self.sim[-1] = np.copy(self.evpoint)
                self.fsim[-1] = f_e

            else:
                self.sim[-1] = np.copy(self.oldevpoint)
                self.fsim[-1] = self.frs
                
            # Iteration done
            self.phase = 2
            
        elif origphase == 4:
            f_oc = result
            
            if f_oc <= self.frs:
                self.sim[-1] = np.copy(self.evpoint)
                self.fsim[-1] = f_oc
                # Iteration Done
                self.phase = 2
                
            else:
                # Shrink
                self.phase = 6
            
        elif origphase == 5:
            f_ic = result

            if f_ic < self.fsim[-1]:
                self.sim[-1] = np.copy(self.evpoint)
                self.fsim[-1] = f_ic
                # Iteration Done
                self.phase = 2
                
            else:
                # Shrink
                self.phase = 6
        
        elif origphase == 6:
            # Part of self.phase 6 that should not be executed if the self.phase was just activated,
            #  i.e. storing the result of the previous vertex.
            # The rest is done in step 3 below.
            self.sim[self.shrink_phase] = np.copy(self.evpoint)
            self.fsim[self.shrink_phase] = result # shrink_phase has not been updated yet

        else:
            raise Exception("Unknown self.phase encountered in minimize_neldermead_combined")
            
            
    # 3. Shrink if necessary (Phase 6)
        if self.phase == 6:
            
            self.shrink_phase += 1
            if self.shrink_phase > self.N: 
                # Vertex self.N+1 does not exist (counting starts from 0), 
                #  so the shrinking is done now
                self.shrink_phase = 0
                # Iteration done
                self.phase = 2
                
            else:

                self.evpoint=self.sim[0] + self.sigma * (self.sim[self.shrink_phase] - self.sim[0])
                

    # 4. Sort & Reflect in case an iteration is done (Phase 2)
        if self.phase == 2:
            # Sort
            # ind[m,:]: List of indices that would sort self.fsim[m,:] in ascending order.
            ind = np.argsort(self.fsim)
            # Sort self.fsim and sims in this way, k=0 corresponds to the best vertex after that
            self.fsim = np.take(self.fsim, ind, 0)
            self.sim = np.take(self.sim, ind, 0)

            # Reflect
            self.xbars = np.add.reduce(self.sim[:-1], 0) / self.N
            self.evpoint = (1 + self.rho) * self.xbars - self.rho * self.sim[-1]
            
        #import os
        #print ("Process {0} init with seed {1}".format(os.getpid(),seed))

        self.evpoint=self.apply_bounds(self.evpoint) 
        self.evpoint_decoded=self.decode(self.evpoint, self.evpoint_decoded,  self.origbounds)
    
        return self.evpoint_decoded


    def get_best(self):
        ind = np.argsort(self.fsim)
        self.fsim = np.take(self.fsim, ind, 0)
        self.sim = np.take(self.sim, ind, 0) 
        
        return self.sim[0]

    def get_best_values(self):
        
        ind = np.argsort(self.fsim)
        self.fsim = np.take(self.fsim, ind, 0)
        self.sim = np.take(self.sim, ind, 0) 
        
        return self.fsim[0]



def minimize_neldermead_bounds(func, x0, bounds, rbounds, totpoints, initial_simplices, args, 
                                 maxfev,
                                 xatol, adaptive, decode_func, decode_args):
    """
    Like _minimize_neldermead, but multiple (M) guesses are optimized simultaneously.
    They each consist of the same number (N) of parameters.
    
    func : Combined target function, maps an array "input" of shape (M, N) 
           to an array "output" of shape (M).
           output[i] will be minimized by adjusting the parameters given in
           input[i,:].

    Options
    -------
    maxfev : int
        Maximum allowed number of function evaluations.
    initial_simplices : array_like of shape (M, N + 1, N)
        Initial simplices. 
        ``initial_simplices[m,j,:]`` should contain the coordinates of
        the jth vertex of the ``N+1`` vertices in the mth simplex, where
        ``N`` is the dimension.
    xatol : float, optional
        Absolute error in xopt between iterations that is acceptable for
        convergence.
    adaptive : bool, optional
        Adapt algorithm parameters to dimensionality of problem. Useful for
        high-dimensional minimization [1]_.

    References
    ----------
    .. [1] Gao, F. and Han, L.
       Implementing the Nelder-Mead simplex algorithm with adaptive
       parameters. 2012. Computational Optimization and Applications.
       51:1, pp. 259-277

    """


    maxfun = maxfev

    fcalls, func = wrap_function(func, args)


    M = len(initial_simplices)


    reconstructions = []

    for m in range(M):
        reconstructions.append(OptItem(bounds, rbounds, totpoints, initial_simplices[m], adaptive, decode_func, decode_args[0][m], decode_args[1]))


    Xss = []
    for m in range(M):
        Xss.append(reconstructions[m].get_simplex())
    
    #print(len(sims))
    #print(sims[0].shape)
    
    fsims = []
    for m in range(M):
        fsims.append(np.array(func(Xss[m])))
     
    for m in range(M):
        reconstructions[m].set_errors(fsims[m])

   
    Xs=[]
    for m in range(M):
        Xs.append(reconstructions[m].get_init_point())
    #print(Xs)
    
    iterations = 1
    
    counter=0
    
    while 1:
        #print(counter, evpoints)
        #print(counter, len(evpoints))
        #print("-------- Iteration", counter)
        
        results = func(Xs)
    
        
        Xs = []
        for m in range(M):
            Xs.append(reconstructions[m].make_iteration(results[m]))

        if fcalls[0] >= maxfun:
            break
              
        counter+=1
    

    x=[]
    fval=[]
    for m in range(M):
        x.append(reconstructions[m].get_best())
        fval.append(reconstructions[m].get_best_values())
        

    warnflag = 0


    msg = _status_message['success']

    result = OptimizeResult(fun=fval, nit=iterations, nfev=fcalls[0],
                            status=warnflag, success=(warnflag == 0),
                            message=msg, x=x)

    
    return result
    
    




#############################################################################################d




def start_reconstruction(rec, index, pp):
    
    #print("Process", os.getpid(), "gets", index)

    p_sim, p_nm = pp

    while 1:
        result, is_last = p_nm.recv()
        #print("Process", os.getpid(), "gets result", result)
        if is_last:
            p_nm.close()
            break
        
        X = rec.make_iteration(result)
        
        #print("Process", os.getpid(), "sends evpoint", evpoint[:3])        
        p_nm.send(X)

    return rec







def minimize_neldermead_bounds_multi(func, x0, bounds, rbounds, totpoints, initial_simplices, args, 
                                 maxfev,
                                 xatol, adaptive,decode_func, decode_args, pool):
    """
    Like _minimize_neldermead, but multiple (M) guesses are optimized simultaneously.
    They each consist of the same number (N) of parameters.
    
    func : Combined target function, maps an array "input" of shape (M, N) 
           to an array "output" of shape (M).
           output[i] will be minimized by adjusting the parameters given in
           input[i,:].

    Options
    -------
    maxfev : int
        Maximum allowed number of function evaluations.
    initial_simplices : array_like of shape (M, N + 1, N)
        Initial simplices. 
        ``initial_simplices[m,j,:]`` should contain the coordinates of
        the jth vertex of the ``N+1`` vertices in the mth simplex, where
        ``N`` is the dimension.
    xatol : float, optional
        Absolute error in xopt between iterations that is acceptable for
        convergence.
    adaptive : bool, optional
        Adapt algorithm parameters to dimensionality of problem. Useful for
        high-dimensional minimization [1]_.

    References
    ----------
    .. [1] Gao, F. and Han, L.
       Implementing the Nelder-Mead simplex algorithm with adaptive
       parameters. 2012. Computational Optimization and Applications.
       51:1, pp. 259-277

    """


    maxfun = maxfev

    fcalls, func = wrap_function(func, args)


    M = len(initial_simplices)


    reconstructions = []

    for m in range(M):
        reconstructions.append(OptItem(bounds, rbounds, totpoints, initial_simplices[m], adaptive, decode_func, decode_args[0][m], decode_args[1]))


    Xss = []
    for m in range(M):
        Xss.append(reconstructions[m].get_simplex())
    
    #print(len(sims))
    #print(sims[0].shape)
    
    fsims = []
    for m in range(M):
        fsims.append(np.array(func(Xss[m])))
     
    for m in range(M):
        reconstructions[m].set_errors(fsims[m])

   
    Xs=[]
    for m in range(M):
        Xs.append(reconstructions[m].get_init_point())
    #print(Xs)
    
    iterations = 1

    

    """
    Structure of the while-loop: 
        1. Evaluate func at the new vertices
        2. Continue the algorithm until a new function evaluation is needed,
            or until an iteration is done
        3. Shrink if necessary (Phase 6)
        4. Sort & Reflect in case an iteration is done (Phase 2)
    """
    pipes =  []
    for m in range(M):
        pipes.append(Pipe())

    p_sim=[]
    p_nm=[]
    for m in range(M):
        po, pi = pipes[m]
        p_sim.append(po)
        p_nm.append(pi)

    arguments = []
    for m in range(M):
        arguments.append((reconstructions[m], m, pipes[m]))  
    
    
    
    
    async_results = pool.starmap_async(start_reconstruction, arguments,1)
    
    counter=0
    
    while 1:
        #print(counter, evpoints)
        #print(counter, len(evpoints))
        #print("-------- Iteration", counter)
        results = func(Xs)
        
        is_last=False
        if  fcalls[0] >= maxfun:
            is_last=True
        
        for m in range(M):
            #print("Master sends result", results[m])        
            p_sim[m].send([results[m], is_last])

        if is_last:
            p_sim[m].close()
            break
        
        #print(Xs)
        Xs = []
        for m in range(M):
            Xs.append(p_sim[m].recv())
            #print("Master receives evpoint", evpoints[m][:3])        
        counter+=1
    
    reconstructions=async_results.get(None)


    x=[]
    fval=[]
    for m in range(M):
        x.append(reconstructions[m].get_best())
        fval.append(reconstructions[m].get_best_values())
        

    warnflag = 0


    msg = _status_message['success']

    result = OptimizeResult(fun=fval, nit=iterations, nfev=fcalls[0],
                            status=warnflag, success=(warnflag == 0),
                            message=msg, x=x)

    
    return result
    
    




