import numpy as np
import h5py
import matplotlib.pyplot as plt


def resize(origpattern, origmask,  newsize):

    outdata = np.zeros([newsize,newsize])   
    outmask = np.zeros([newsize,newsize])   

    tempweight = np.zeros([newsize,newsize])  

    scalefactor = origpattern.shape[0]/newsize 
        
    for y in range(0, origpattern.shape[1]):
        for x in range(0, origpattern.shape[0]):
            if origmask[x,y]>0:
                ix = x/scalefactor
                iy = y/scalefactor
                
                ixt = int(np.floor(ix))
                iyt = int(np.floor(iy))

                xtdec = ix-ixt
                ytdec = iy-iyt
                
                totweight = 0
                val = origpattern[x,y]
                if 1: #val>=0:
                    if ixt>=0 and iyt>=0 and ixt<newsize and iyt<newsize :
                        weight = (1.-xtdec)*(1.-ytdec)
                        outdata[ixt, iyt]+= val*weight
                        tempweight[ixt, iyt]+= weight
                        
                    
                    if ixt+1>=0 and iyt>=0 and ixt+1<newsize and iyt<newsize :
                        weight = (xtdec)*(1.-ytdec)
                        outdata[ixt+1, iyt]+= val*weight
                        tempweight[ixt+1, iyt]+= weight
                    
                    if ixt>=0 and iyt+1>=0 and ixt<newsize and iyt+1<newsize :
                        weight = (1.-xtdec)*(ytdec)
                        outdata[ixt, iyt+1]+= val*weight
                        tempweight[ixt, iyt+1]+= weight
                    
                    if ixt+1>=0 and iyt+1>=0 and ixt+1<newsize and iyt+1<newsize :
                        weight = (xtdec)*(ytdec)
                        outdata[ixt+1, iyt+1]+= val*weight
                        tempweight[ixt+1, iyt+1]+= weight
                    
                
    maxw = np.amax(tempweight)

    for y in range(0, outdata.shape[1]):
        for x in range(0, outdata.shape[0]):
            if tempweight[x,y]>0.5*maxw:
                outdata[x,y] /=tempweight[x,y]
                outmask[x,y]=1
            else:
                outdata[x,y]=-1;            
                outmask[x,y]=0;
                
                        
    return outdata, outmask
             
        
    



def cut(data, center, size):
    
    newdata = np.copy(data[center[0]-size//2:center[0]+size//2, center[1]-size//2:center[1]+size//2])
    
    return newdata

def autocut(data, mask, satmask, center, size):
    
    xdim=data.shape[0]
    ydim=data.shape[1]
    
    xmax=2*center[0]
    if 2*(xdim-center[0])>xmax:
        xmax=2*(xdim-center[0])

    ymax=2*center[1]
    if 2*(ydim-center[1])>ymax:
        ymax=2*(ydim-center[1])
    
    maxdim=xmax
    if maxdim<ymax:
        maxdim=ymax
        
    newdata=np.ones([maxdim, maxdim])*-1
    newmask=np.zeros([maxdim, maxdim])
    newsatmask=np.zeros([maxdim, maxdim])
    
    
    newdata[maxdim//2-center[0]: maxdim//2-center[0]+data.shape[0], maxdim//2-center[1]: maxdim//2-center[1]+data.shape[1]]=np.copy(data)
    newmask[maxdim//2-center[0]: maxdim//2-center[0]+mask.shape[0], maxdim//2-center[1]: maxdim//2-center[1]+mask.shape[1]]=np.copy(mask)
    newsatmask[maxdim//2-center[0]: maxdim//2-center[0]+satmask.shape[0], maxdim//2-center[1]: maxdim//2-center[1]+satmask.shape[1]]=np.copy(satmask)
    
    return newdata, newmask, newsatmask


def read_data(dataname, center, cutsize, newsize, detdistance, pixelsize, satlevel, minlevel = 0):
    
    f = h5py.File(dataname, 'r')
    dataorig = np.array(f['data'][()], dtype=float)
    maskorig = np.array(f['mask'][()], dtype=float)
    dataorig[512:, 512:]*=1.53
    dataorig[845:, 512:]/=1.5

    maskorig[510:592,:]=1.
    
    
    maskorig[maskorig>0.5]=1.
    maskorig[maskorig<=0.5]=0.
    
    maskorig=1.-maskorig
    
    satmaskorig=np.zeros(maskorig.shape)
        
    satmaskorig[dataorig>satlevel]=1.

    data=[]
    mask=[]
    satmask = []
    angle=0
    
    if cutsize>0:
        data = cut(dataorig, center, cutsize)
        mask = cut(maskorig, center, cutsize)
        satmask = cut(satmaskorig, center, cutsize)

        angle = np.arctan2(cutsize/2.*pixelsize, detdistance)/np.pi*180.

    else:
        data, mask, satmask= autocut(dataorig, maskorig,satmaskorig, center, cutsize)
        angle = np.arctan2(data.shape[0]/2.*pixelsize, detdistance)/np.pi*180.
    
    
    
    mask[np.logical_and(satmask>0.5, mask==1)]=0
    

    print(data.shape)
    if newsize>0:
        data, mask=resize(data, mask, newsize)


    data[data<0]=0

    return data,  mask, angle


